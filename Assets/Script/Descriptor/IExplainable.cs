/// <summary>
/// 설명 텍스트를 가지는 모든 클래스가 상속받는 인터페이스
/// Descriptor와 연계된다
/// </summary>
public interface IExplainable
{
    public string Explain();
}