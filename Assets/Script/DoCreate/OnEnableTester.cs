using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

[CreateAssetMenu(fileName = "OnEnableTester", menuName = "ScriptableObjects/OnEnableTester")]
public class OnEnableTester : ScriptableObject, IExplainable
{
    public Descriptor descriptor = new Descriptor();
    public int id;
    public UseEffect[] effects;

    public void OnEnable()
    {
        Debug.Log("OnEnable");
        descriptor.InitDescriptor(this);
    }

    public string Explain()
    {
        return "Explain!!!";
    }
}