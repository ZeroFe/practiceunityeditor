[System.Serializable]
public class UseEffectReference : ScriptableObjectReference<UseEffect>
{
    public UseEffect Effect
    {
        get 
        {
            return (UseEffect)reference;
        }
    }
}