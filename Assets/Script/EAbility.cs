public enum EAbility
{
    Attack,
    AttackSpeed,
    MaxHp,
    HpRegen,
    Defence,
    Speed,
    SkillCoolTime,
    DamageReduceRate,
    SkillRange,
    AttackPercent,
}
