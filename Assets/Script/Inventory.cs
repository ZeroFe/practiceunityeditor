using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UniRx;

namespace MagiMaker
{

    public class ChangeConsumableEvent : UnityEvent<Sprite, string, int> { }

    [System.Serializable]
    public class CInventory
    {
        // int에 들어가는 값은 Item Code
        public class AddItemEvent : UnityEvent<int> { }
        public AddItemEvent addItemEvent { get; set; } = new AddItemEvent();

        //int에 들어가는 값은 Earned Gold Amount
        public class AddGoldEvent : UnityEvent<int> { }
        public AddGoldEvent addGoldEvent { get; set; } = new AddGoldEvent();
        public class SubGoldEvent : UnityEvent<int> { }

        public SubGoldEvent subGoldEvent = new SubGoldEvent();


        public UnityEvent UseConsumableEvent { get; set; } = new UnityEvent();

        public class ConsumableItemSlot
        {
            public ConsumableItem item;
            public int stack;

            public ConsumableItemSlot(ConsumableItem newConsumable)
            {
                item = newConsumable;
                stack = 1;
            }
        }

        public class EquipItemSlot
        {
            public EquipItem item;
            public int equipPassiveCount;

            public EquipItemSlot(EquipItem newEquip)
            {
                item = newEquip;
                equipPassiveCount = 0;
            }
        }

        public List<EquipItemSlot> EquipItems { get; private set; } = new List<EquipItemSlot>();
        public List<ConsumableItemSlot> ConsumableItems { get; private set; } = new List<ConsumableItemSlot>();

        private GameObject _inventoryUser;

        public int Gold
        {
            get { return _gold; }
            set { _gold = value; }
        }
        private int _gold;

        private int[] _equipAbilityIncreaseSizeArr = new int[Enum.GetValues(typeof(EAbility)).Length];
        public int[] equipAbilityIncreaseSizeArrPublic = new int[Enum.GetValues(typeof(EAbility)).Length];

        public ChangeConsumableEvent changeConsumableEvent = new ChangeConsumableEvent();

        public CInventory(GameObject userObject, int equipCapacity = 10, int consumableCapacity = 3, int gold = 0)
        {
            _inventoryUser = userObject;
            EquipItems.Capacity = equipCapacity;
            ConsumableItems.Capacity = consumableCapacity;
            _gold = gold;

            RegisterEquipEvent();
        }

        #region Implement Passive, Upgrade
        private void RegisterEquipEvent()
        {
            var playerStat = _inventoryUser.GetComponent<Player>();
            _inventoryUser.GetComponent<Player>().OnGiveDamage.Subscribe((data) => CallItemEvent(data.eventType, data.damageAmount));
        }

        /// <summary>
        /// 해당 이벤트가 일어나면 장비 효과 발동(패시브, 성장)
        /// </summary>
        /// <param name="condition">패시브 발동 조건</param>
        /// <param name="count">패시브 조건 인자</param>
        private void CallItemEvent(EventObject eventType, int addedCount)
        {
            foreach (var equipSlot in EquipItems)
            {
                if (equipSlot.item.passiveCondition.conditionEvent == eventType)
                {
                    ExecuteEquipPassive(equipSlot, addedCount);
                }
            }
        }

        /// <summary>
        /// 장비 패시브를 추가 조건에 따라 실행
        /// ex) n회에 한 번 실행 / n 이상일 때 실행 / n 이하일 때 실행
        /// </summary>
        /// <param name="equipSlot">적용 장비</param>
        /// <param name="addedCount"></param>
        private void ExecuteEquipPassive(EquipItemSlot equipSlot, int addedCount)
        {
            equipSlot.equipPassiveCount = equipSlot.item.passiveCondition.conditionOption.AddCount(equipSlot.equipPassiveCount, addedCount);
            if (equipSlot.item.passiveCondition.SatisfyCondition(equipSlot.item.passiveCount, equipSlot.equipPassiveCount))
            {
                foreach (var item in equipSlot.item.passiveEffect)
                {
                    item.GetReference().TakeUseEffect();
                }
            }
            equipSlot.equipPassiveCount = equipSlot.item.passiveCondition.NextUserCount(equipSlot.equipPassiveCount, addedCount);
        }
        #endregion

        #region Add / Delete Item
        public bool AddItem(Item newItem)
        {
            if (newItem is EquipItem)
            {
                return AddEquip(newItem as EquipItem);
            }
            else if (newItem is ConsumableItem)
            {
                return AddConsumableItem(newItem as ConsumableItem);
            }
            else
            {
                Debug.Log("Can't Add Unknown Type Item");
                return false;
            }
        }

        /// <summary>
        /// 가방에 장비 아이템 넣기
        /// </summary>
        /// <returns>아이템 넣기 성공했는지</returns>
        private bool AddEquip(EquipItem newEquip)
        {
            if (EquipItems.Count >= EquipItems.Capacity)
            {
                return false;
            }

            EquipItems.Add(new EquipItemSlot(newEquip));
            AddEquipAbility(newEquip);

            addItemEvent.Invoke(newEquip.name.GetHashCode());

            return true;
        }

        /// <summary>
        /// 가방에 소비 아이템 넣기
        /// </summary>
        /// <returns>아이템 넣기 성공했는지</returns>
        private bool AddConsumableItem(ConsumableItem newItem)
        {
            if (ConsumableItems.Count >= ConsumableItems.Capacity)
            {
                return false;
            }

            int overlapIndex;
            if ((overlapIndex = HasEqualConsumable(newItem)) != -1)
            {
                ConsumableItems[overlapIndex].stack++;
            }
            else
            {
                ConsumableItems.Add(new ConsumableItemSlot(newItem));
            }

            // 이벤트 발생용
            //SelectedConsumableNumber = SelectedConsumableNumber;

            return true;
        }

        /// <summary>
        /// 소비 아이템 중복인지 확인 및 중복인 소비 아이템 인덱스 리턴
        /// </summary>
        /// <param name="newConsumable"></param>
        /// <returns></returns>
        private int HasEqualConsumable(ConsumableItem newConsumable)
        {
            for (int index = 0; index < ConsumableItems.Count; index++)
            {
                if (ConsumableItems[index].item.name == newConsumable.name)
                {
                    return index;
                }
            }
            return -1;
        }
        #endregion

        #region Earn / Lost Gold
        /// <summary>
        /// amount만큼 골드를 획득하고 isInvokeEvent에 따라 골드 획득 이벤트를 호출한다
        /// </summary>
        /// <param name="amount">획득량</param>
        /// <param name="isInvokeEvent">골드 획득 이벤트 발생 여부</param>
        public void AddGold(int amount, bool isInvokeEvent)
        {
            Debug.Log("before Gold = " + _gold);

            _gold += amount;

            if (isInvokeEvent)
                addGoldEvent?.Invoke(amount);

            Debug.Log("after Gold = " + _gold);
        }

        public void SubGold(int gold, bool isSubGoldEvent)
        {
            Debug.Log("before Gold = " + _gold);

            if (isSubGoldEvent)
            {
                subGoldEvent?.Invoke(gold);
                return;
            }

            _gold -= gold;


            Debug.Log("after Gold = " + _gold);
        }
        #endregion

        //public void GetNextConsumable()
        //{
        //    if (ConsumableItems.Count == 0)
        //    {
        //        Debug.Log("Consumable inventory is empty");
        //        return;
        //    }

        //    ++SelectedConsumableNumber;
        //}

        //public void UseSelectedConsumable()
        //{
        //    if (ConsumableItems.Count == 0)
        //    {
        //        Debug.Log("consumable empty");
        //        return;
        //    }

        //    Debug.Log($"Selected Consumable Number : {SelectedConsumableNumber}");
        //    UseConsumable(ConsumableItems[SelectedConsumableNumber].consumable);
        //    if (--ConsumableItems[SelectedConsumableNumber].stack <= 0)
        //    {
        //        ConsumableItems.RemoveAt(SelectedConsumableNumber);
        //        SelectedConsumableNumber = 0;
        //    }
        //}

        #region EquipAbility
        public int GetEquipAbilityIncreaseSize(EAbility ability)
        {
            return _equipAbilityIncreaseSizeArr[(int)ability];
        }

        /// <summary>
        /// 장비 아이템의 합산 능력치 및 효과 갱신
        /// </summary>
        private void AddEquipAbility(EquipItem equip)
        {
            foreach (var ability in equip.equipAbilities)
            {
                _equipAbilityIncreaseSizeArr[(int)ability.equipEffect] += ability.value;
            }
        }

        public void AddAbility(int value, EAbility type)
        {
            equipAbilityIncreaseSizeArrPublic[(int)type] += value;
        }

        private void DeleteEquipAbility(EquipItem equip)
        {
            foreach (var ability in equip.equipAbilities)
            {
                _equipAbilityIncreaseSizeArr[(int)ability.equipEffect] -= ability.value;
            }
        }
        #endregion


        private void UseConsumable(ConsumableItem consumable)
        {
            //_inventoryUser.GetComponent<CPlayerPara>().TakeUseEffectHandleList(consumable.UseEffectList, _inventoryUser);
            UseConsumableEvent?.Invoke();
        }
    }
}