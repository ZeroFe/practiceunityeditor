﻿using System;
using UnityEngine;

/// <summary>
/// string을 readonly형 TextArea로 만들 때 사용하는 attribute
/// </summary>
[AttributeUsage(AttributeTargets.Field)]
public class ReadOnlyTextAreaAttribute : PropertyAttribute
{
    public readonly int minLines;
    public readonly int maxLines;

    public ReadOnlyTextAreaAttribute(int min, int max)
    {
        minLines = min;
        maxLines = max;
    }
}