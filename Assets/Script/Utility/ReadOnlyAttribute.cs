﻿using System;
using UnityEngine;

/// <summary>
/// 특정 필드를 read-only로 만드는 Attribute
/// property를 그리는 Attribute와 호환되지 않는다
/// </summary>
[AttributeUsage(AttributeTargets.Field)]
public class ReadOnlyAttribute : PropertyAttribute
{
    public readonly bool runtimeOnly;

    public ReadOnlyAttribute(bool runtimeOnly = false)
    {
        this.runtimeOnly = runtimeOnly;
    }
}