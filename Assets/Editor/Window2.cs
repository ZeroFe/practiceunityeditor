using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEditor;

public class Window2 : EditorWindow
{
    List<MemberInfo> members;
    SerializedProperty[] allProperty; 
    Vector2 scrollPosition = Vector2.zero;

    [MenuItem("Magimaker/Example2")]
    static void Open()
    {
        Debug.Log("Opened");

        GetWindow<Window2>();
    }

    private void OnEnable()
    {
        FieldInfo[] allField;
        allField = typeof(EquipItem).GetFields(BindingFlags.Public | BindingFlags.Instance);
        EquipItem item = CreateInstance<EquipItem>();
        var so = new SerializedObject(item);
        allProperty = new SerializedProperty[allField.Length];
        for (int i = 0; i < allField.Length; i++)
        {
            allProperty[i] = so.FindProperty(allField[i].Name);
        }
    }

    void OnGUI()
    {
        scrollPosition = GUILayout.BeginScrollView(scrollPosition, false, true, 
            GUILayout.Width(position.width), GUILayout.Height(position.height - EditorGUIUtility.singleLineHeight));
        foreach (var property in allProperty)
        {
            EditorGUILayout.PropertyField(property);
        }
        GUILayout.EndScrollView();
    }
}
