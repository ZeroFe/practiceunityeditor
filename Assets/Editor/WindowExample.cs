using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class WindowExample : EditorWindow
{
    [MenuItem("Magimaker/Example")]
    static void Open()
    {
        var window = GetWindow<WindowExample>();
        window.minSize = new Vector2(300, 300);
        window.Show();
    }

    //인스턴스화
    ExamplePopupContent popupContent = new ExamplePopupContent();

    void OnGUI()
    {
        if (GUILayout.Button("PopupContent", GUILayout.Width(128)))
        {
            var activatorRect = GUILayoutUtility.GetLastRect();
            activatorRect.x -= 100;
            activatorRect.width = 100;
            //Popup을 표시한다
            PopupWindow.Show(activatorRect, popupContent);
        }
    }
}

public class ExamplePopupContent : PopupWindowContent
{
    public override void OnGUI(Rect rect)
    {
        EditorGUILayout.LabelField("Lebel");
    }

    public override void OnOpen()
    {
        Debug.Log("표시할 때에 호출됨");
    }

    public override void OnClose()
    {
        Debug.Log("닫을때 호출됨");
    }

    public override Vector2 GetWindowSize()
    {
        //Popup 의 사이즈
        return new Vector2(100, 100);
    }
}