using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Reflection;

public class ExampleWizard : ScriptableWizard
{
    public string gameObjectName;
    List<MemberInfo> members;
    SerializedProperty[] allProperty;

    [MenuItem("Magimaker/Wizard")]
    static void Open()
    {
        DisplayWizard<ExampleWizard>("Example Wizard");
        MemberInfo[] allMember;
        allMember = typeof(EquipItem).GetMembers();
        EquipItem equip = CreateInstance<EquipItem>();
        var so = new SerializedObject(equip);
        foreach (var member in allMember)
        {
            so.FindProperty(member.Name);
        }
    }

    void OnWizardUpdate()
    {
        //Debug.Log("Update");
    }

    protected override bool DrawWizardGUI()
    {
        base.DrawWizardGUI();
        foreach (var prop in allProperty)
        {
            EditorGUILayout.PropertyField(prop);
        }

        //false 를 반환하면 OnWizardUpdate 가 호출되지 않게 됩니다
        return true;
    }

    private void OnWizardCreate()
    {
        //AssetDatabase.CreateAsset();
    }
}
