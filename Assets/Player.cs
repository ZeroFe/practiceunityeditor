using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class Player : MonoBehaviour
{
    /// <summary>
    /// 데미지를 주는 행위에 관한 이벤트
    /// int : 체력 변화량
    /// GameObject : 누구한테 주었는지
    /// </summary>
    [System.Serializable]
    public struct GiveDamageData
    {
        public MagiMaker.EventObject eventType;
        public int damageAmount;
    }
    public Subject<GiveDamageData> OnGiveDamage = new Subject<GiveDamageData>();
    public MagiMaker.EventObject giveDamageType = null;

}
