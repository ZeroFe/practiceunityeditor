using System.Text;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

public class LogWriter : MonoBehaviour
{
    private void Start()
    {
        var stringBuilder = new StringBuilder();

        //var myLoadedAssetBundle = AssetBundle.LoadFromFile("Assets/Item");
        //foreach (var item in myLoadedAssetBundle.LoadAllAssets<Item>())
        var items = Resources.LoadAll<Item>("Item");
        foreach (var item in items)
        {
            stringBuilder.AppendLine($"{item.name} : {item.ID}");
        }

        GetComponent<Text>().text = stringBuilder.ToString();
    }
}
